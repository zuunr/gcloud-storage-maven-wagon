# gcloud-storage-maven-wagon

![Apache License](https://img.shields.io/static/v1.svg?label=License&message=Apache-2.0&color=%230af)
[![Maven Central](https://img.shields.io/maven-central/v/com.zuunr.util/gcloud-storage-maven-wagon.svg?label=Maven%20Central&color=%230af)](https://search.maven.org/search?q=a:gcloud-storage-maven-wagon)

This module adds support for using a Google Cloud Storage (*gcs://*) as a remote maven repository for your maven projects.

## Supported tags

* [`1.0.0.M1-080be85`, (*080be85/pom.xml*)](https://bitbucket.org/zuunr/gcloud-storage-maven-wagon/src/080be85/pom.xml)

## Usage

To use this module, add this artifact as an extension in your maven project, replacing 1.0.0-abcdefg with a supported tag:

```xml
<build>
    <extensions>
        <extension>
            <groupId>com.zuunr.util</groupId>
            <artifactId>gcloud-storage-maven-wagon</artifactId>
            <version>1.0.0-abcdefg</version>
        </extension>
    </extensions>
...
</build>
```

After that you can now add a distributionManagement or a repository with the *gcs://* protocol to your maven project.

```xml
<repositories>
    <repository>
        <id>google</id>
        <url>gcs://my-maven-repository/release</url>
    </repository>
</repositories>
```

## Configuration

The module requires an environment variable named GOOGLE\_APPLICATION\_CREDENTIALS to be set and pointing to a Google service account file (json) with appropriate storage privileges.
