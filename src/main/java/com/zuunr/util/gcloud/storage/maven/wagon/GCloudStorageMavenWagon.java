/*
 * Copyright 2018 Zuunr AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.zuunr.util.gcloud.storage.maven.wagon;

import java.io.File;
import java.nio.file.Files;

import org.apache.maven.wagon.AbstractWagon;
import org.apache.maven.wagon.ConnectionException;
import org.apache.maven.wagon.ResourceDoesNotExistException;
import org.apache.maven.wagon.TransferFailedException;
import org.apache.maven.wagon.authentication.AuthenticationException;
import org.apache.maven.wagon.authorization.AuthorizationException;

import com.google.cloud.storage.Blob;
import com.google.cloud.storage.BlobId;
import com.google.cloud.storage.BlobInfo;
import com.google.cloud.storage.Storage;
import com.google.cloud.storage.StorageOptions;

/**
 * <p>The GCloudStorageMavenWagon is a Maven wagon implementation which
 * adds support for using a Google Cloud Storage as Maven repository.</p>
 * 
 * <p>The class enables connecting to a storage bucket by utilizing the 
 * Google Cloud Storage library.</p>
 *
 * @author Mikael Ahlberg
 */
public class GCloudStorageMavenWagon extends AbstractWagon {
    
    private Storage storage;
    
    @Override
    public void get(String resourceName, File destination) throws TransferFailedException, ResourceDoesNotExistException, AuthorizationException {
        Blob blob = getRemoteBlob(resourceName);
        
        blob.downloadTo(destination.toPath());
    }
    
    @Override
    public boolean getIfNewer(String resourceName, File destination, long timestamp) throws TransferFailedException, ResourceDoesNotExistException, AuthorizationException {
        Blob blob = getRemoteBlob(resourceName);
        
        if (blob.getUpdateTime() > timestamp) {
            blob.downloadTo(destination.toPath());
            
            return true;
        }
        
        return false;
    }
    
    @Override
    public void put(File source, String destination) throws TransferFailedException, ResourceDoesNotExistException, AuthorizationException {
        BlobId blobId = BlobId.of(getBucketName(), getAbsoluteRemotePath(destination));
        BlobInfo blobInfo = BlobInfo.newBuilder(blobId).build();
        
        try {
            storage.create(blobInfo, Files.readAllBytes(source.toPath()));
        } catch (Exception e) {
            throw new TransferFailedException("Could not read/transfer file to remote destination: " + destination, e);
        }
    }
    
    @Override
    protected void openConnectionInternal() throws ConnectionException, AuthenticationException {
        try {
            storage = StorageOptions.getDefaultInstance().getService();
        } catch (Exception e) {
            throw new ConnectionException("Could not connect to google cloud storage", e);
        }
    }
    
    @Override
    protected void closeConnection() throws ConnectionException {
        storage = null;
    }
    
    private Blob getRemoteBlob(String resourceName) throws ResourceDoesNotExistException {
        Blob blob = storage.get(BlobId.of(getBucketName(), getAbsoluteRemotePath(resourceName)));
        
        if (blob == null) {
            throw new ResourceDoesNotExistException("The resource does not exist: " + resourceName);
        }
        
        return blob;
    }
    
    private String getBucketName() {
        return repository.getHost();
    }
    
    private String getAbsoluteRemotePath(String resourceName) {
        String baseDirectory = repository.getBasedir();
        
        StringBuilder absolutePath = new StringBuilder();
        
        if (baseDirectory.startsWith("/")) {
            absolutePath.append(baseDirectory.substring(1));
        } else {
            absolutePath.append(baseDirectory);
        }
        
        if (!baseDirectory.endsWith("/")) {
            absolutePath.append("/");
        }
        
        return absolutePath.append(resourceName).toString();
    }
}
